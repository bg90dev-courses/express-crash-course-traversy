# Express Crash Course Traversy

![Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![](https://img.shields.io/badge/Status-finished-purple.svg)

Curso de Express de Traversy Media https://www.youtube.com/watch?v=L72fhGm1tfE

## Built With
<a href="https://www.javascript.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript"></a>
<a href="https://nodejs.org/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/nodejs.png" width=50 alt="NodeJS"></a>
<a href="https://expressjs.com/es/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/expressJS.png" width=50 alt="ExpressJS"></a>

### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author
* **Borja Gete** - <borjag90dev@gmail.com>